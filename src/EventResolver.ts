import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { AdvancedFormItemResolver } from '@universis/forms';

@Injectable()
export class EventResolver extends AdvancedFormItemResolver {
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
      ): Promise<any> | any {
        return super.resolve(route, state).then((item: any) => {
          Object.assign(item, {
            willBeScheduled: (item.startDate == null && item.endDate == null)
          });
          if (item.duration) {
            Object.assign(item, {
              totalDuration: moment.duration(item.duration).asMinutes()
            });
          }
          // todo: remove startTime and endTime attributes
          if (item.eventHoursSpecification == null) {
            if (item.startDate) {
              Object.assign(item, {
                startTime: item.startDate
              });
            }
            if (item.endDate) {
              Object.assign(item, {
                endTime: item.endDate
              });
            }
          }
          return item;
        });
      }
}
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { combineLatest, Subscription } from 'rxjs';
import { EventsService } from '../../events.service';


@Component({
  selector: 'universis-new-event-container',
  template: ''
})
export class NewEventContainerComponent implements OnInit, OnDestroy {

  private activatedRouteSubscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _eventsService: EventsService) { }

  ngOnInit() {
    this.activatedRouteSubscription = combineLatest(
      this.activatedRoute.parent.params,
      this.activatedRoute.params,
      this.activatedRoute.data).subscribe(([parentParams, params, routeData]) => {
        const { data, ...rest } = routeData;
        if(routeData.model === 'CourseExamEvents') {
          this.newExamEvent({...params, ...data, ...rest });
        } else {
          this.newEvent({ ...params, ...data, ...rest});
        }
      });
  }

  async newEvent(data: any) {
    const { timetable, courseClass, instructor, model, action, continueLink = '../../', recursive = false } = data;
    const formProperties: any = {};

    if (courseClass && instructor) {
      formProperties.mustRegisterSection = courseClass.mustRegisterSection;
      formProperties.sectionsEndpoint = `CourseClassSections?$filter=courseClass eq '${courseClass.id}' and instructors/instructor eq '${instructor.id}'`;
    }

    if (timetable) {
      formProperties.placesEndpoint = `TimetableEvents/${timetable}/availablePlaces`;
      
      if (recursive) {
        const timetableData = await this._context.model('TimetableEvents').where('id').equal(timetable).getItem();
        formProperties.timetableStartDate = timetableData.startDate;
        formProperties.timetableEndDate = timetableData.endDate; 
      }
    }

    this._eventsService.newEvent({
      formProperties,
      formData: { title: courseClass && courseClass.title, superEvent: timetable || null, instructor:instructor || null, courseClass: courseClass || null },
      formSrc: `${model}/${action}`,
      continueNavigation: {
        continueLink: continueLink,
        navigationExtras: {
          relativeTo: this.activatedRoute
        }
      },
      execute: (submissionData) => {
        const parsedData = this._eventsService.parseFormData(submissionData, {
          courseClass: courseClass,
          instructor: instructor,
          recursive: recursive
        });
        return this._context.model(model).save(parsedData);
      }
    });
  }

  async newExamEvent(data: any) {
    const { timetable, id, model, action, continueLink = '../../', recursive = false } = data;
    const formProperties: any = {};

    if (timetable) {
      formProperties.placesEndpoint = `TimetableEvents/${timetable}/availablePlaces`;
    }
    const courseExam = await this._context.model('CourseExams').where('id').equal(id).expand('course').getItem();
    this._eventsService.newEvent({
      formProperties,
      formData: { courseExam: courseExam, superEvent: timetable || null, title: courseExam && courseExam.name },
      formSrc: `${model}/${action}`,
      continueNavigation: {
        continueLink: continueLink,
        navigationExtras: {
          relativeTo: this.activatedRoute
        }
      },
      execute: (submissionData) => {
        const parsedData = this._eventsService.parseFormData(submissionData, {
        });
        return this._context.model(model).save(parsedData);
      }
    });
}
  ngOnDestroy(): void {
    if (this.activatedRouteSubscription) {
      this.activatedRouteSubscription.unsubscribe();
    }
  }
}

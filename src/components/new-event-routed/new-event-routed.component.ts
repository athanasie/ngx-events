import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { RouterModalOkCancel } from '@universis/common/routing';
import { NewEventComponent } from '../new-event/new-event.component';

/*
This is a draft component. If it's implemented further, it can replace new-event-container component
*/

@Component({
  selector: 'app-new-event-routed',
  templateUrl: './new-event-routed.component.html'
})
export class NewEventRoutedComponent extends RouterModalOkCancel implements OnInit {
  @ViewChild('newEventComponent') newEventComponent: NewEventComponent;

  constructor(router: Router,
    activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
  }

  ngOnInit() {
  }

  cancel(): Promise<any> {
    return super.close();
  }

  async ok(): Promise<any> {
    const submissionData = this.newEventComponent.formComponent.form.formio.data;
    await this._context.model('Events').save(submissionData);
    return super.close();
  }

}

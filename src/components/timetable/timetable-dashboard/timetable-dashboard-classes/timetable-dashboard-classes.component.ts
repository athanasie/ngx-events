import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { AdvancedTableDataResult } from '@universis/ngx-tables';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-timetable-dashboard-classes',
  templateUrl: './timetable-dashboard-classes.component.html'
})
export class TimetableDashboardClassesComponent implements OnInit, OnDestroy {

  public isLoading: boolean;
  public timetableEvent: any;
  public recordsTotal: any;
  public readonly tableConfigSrc = 'assets/lists/CourseClassInstructors/active.json';
  public readonly searchConfigSrc = 'assets/lists/CourseClassInstructors/search.active.json';
  private paramSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  ngOnInit(): void {
    this.paramSubscription = this._activatedRoute.params.subscribe(async ({ timetable }) => {
      this.isLoading = true;
      this.timetableEvent = await this._context.model('TimetableEvents')
        .where('id').equal(timetable)
        .expand('academicPeriods')
        .getItem();
      this.isLoading = false;
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  onTableLoading({ target = null } = {}) {
    // set default filter
    if (target && target.config) {
      const { organizer, academicYear, academicPeriods } = this.timetableEvent;
      // create query
      const q = this._context.model('CourseClassInstructors').asQueryable();
      q.where('courseClass/department').equal(organizer)
        .and('courseClass/year').equal(academicYear);
      if (Array.isArray(academicPeriods) && academicPeriods.length) {
        q.and('courseClass/period').equal(academicPeriods.map((x: any) => x.id));
      }
      // and get filter
      const { $filter: filter } = q.getParams();
      target.config.defaults.filter =  filter;
    }
  }

  onSearchLoading({ target = null } = {}) {
    const { academicPeriods } = this.timetableEvent;

    // if the timetable includes both periods, allow users to filter periods
    if (target && target.form && academicPeriods && academicPeriods.length > 1) {
      target.form.searchPeriods = true;
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { AdvancedTableDataResult } from '@universis/ngx-tables';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-timetable-dashboard-exams',
  templateUrl: './timetable-dashboard-exams.component.html'
})
export class TimetableDashboardExamsComponent implements OnInit {

  public isLoading: boolean;
  public timetableEvent: any;
  public recordsTotal: any;
  public readonly tableConfigSrc = 'assets/lists/CourseExams/active.json';
  public readonly searchConfigSrc = 'assets/lists/CourseExams/search.active.json';
  private paramSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  ngOnInit(): void {
    this.paramSubscription = this._activatedRoute.params.subscribe(async ({ timetable }) => {
      this.isLoading = true;
      this.timetableEvent = await this._context.model('TimetableEvents')
        .where('id').equal(timetable)
        .expand('examPeriods')
        .getItem();
      this.isLoading = false;
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  onTableLoading({ target = null } = {}) {
    // set default filter
    if (target && target.config) {
      const { organizer, academicYear, examPeriods } = this.timetableEvent;
      // create query
      const q = this._context.model('CourseExams').asQueryable();
      q.where('course/department').equal(organizer)
        .and('year').equal(academicYear);
      if (Array.isArray(examPeriods) && examPeriods.length) {
        q.and('examPeriod').equal(examPeriods.map((x: any) => x.id));
      } 
      // case where examPeriods are not defined apply a false filter
      else {
        q.and('examPeriod').equal(0);
      }
      // and get filter
      const { $filter: filter } = q.getParams();
      target.config.defaults.filter =  filter;
    }
  }

  onSearchLoading({ target = null } = {}) {
    const { examPeriods } = this.timetableEvent;

    // if the timetable includes both periods, allow users to filter periods
    if (target && target.form && examPeriods && examPeriods.length > 1) {
      target.form.searchPeriods = true;
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}

import { Component, ElementRef, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, AppEventService } from '@universis/common';
import { AdvancedFormModalComponent, AdvancedFormsService } from '@universis/forms';
import { EventsService } from '../../events.service';

@Component({
  selector: 'app-events-edit',
  templateUrl: './events-edit.component.html'
})
export class EventsEditComponent extends AdvancedFormModalComponent {

  constructor(
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private context: AngularDataContext,
    private formService: AdvancedFormsService,
    private translateService: TranslateService,
    private errorService: ErrorService,
    private element: ElementRef,
    private appEvent: AppEventService,
    private _eventsService: EventsService) {
    super(router, activatedRoute, context, formService, translateService, errorService, element, appEvent);
  }

  async ok() {
    const data = this.form.submission.data;
    this.form.submission.data = this._eventsService.parseFormData(data, { recursive: !!data.eventHoursSpecification })[0];
    super.ok()
  }

}
